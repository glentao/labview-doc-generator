---
title: "Version 1.0 is released!"
date: 2020-07-15T15:48:10+02:00
tags: ["release"]
draft: false
---

Here it is! We are glad to announce the release of the first official version of Antidoc.

We are firmly convinced that Antidoc will help you get a valuable and up to date documentation for your LabVIEW project.

Even if we want this tool to be useful to most LabVIEW developers, this first version addresses, mainly DQMH(R) framework users.
That said, what works for DQMH(R) will work in the future for other frameworks.

## What will you get with Antidoc?

### Beautiful DQMH(R) relationship modules graph

This graph shows the different DQMH modules used in your project and how they share data using Requests and Broadcasts.

![Antidoc relationship graph](https://wovalab.gitlab.io/open-source/labview-doc-generator/doc/Antidoc-Output-v1.0/Images/diag-5fea0a74e9493cdf58b8029648e033ee.png)
<!--more-->
### Complete DQMH(R) module description

Each DQMH has its description with module type, information on who is starting and stopping it, and relationships with other parts of the code.

![Antidoc DQMH Module description](https://wovalab.gitlab.io/open-source/labview-doc-generator/img/Output-dqmhModuleDescription.png)

### Beautiful and full-featured final documents

The generated document has internal links to help you navigate through it.

A table of content is available at the beginning of the document,

![Antidoc Table of Content](https://wovalab.gitlab.io/open-source/labview-doc-generator/img/Output-toc.png)

and internal links allow you to navigate quickly to VI description.

![Antidoc xref](https://wovalab.gitlab.io/open-source/labview-doc-generator/img/Output-xref.png)

You can glance at the final HTML file obtained with Antidoc [here](https://wovalab.gitlab.io/open-source/labview-doc-generator/doc/Antidoc-Output-v1.0/Project-Documentation.html)

If you don't like the default theme used to generate final files, you can customize the rendering toolchain to your desired look and feel.
You can, for example, obtain this kind of [PDF](https://wovalab.gitlab.io/open-source/labview-doc-generator/doc/Antidoc-Output-v1.0/Project-Documentation.pdf).

## How will you get it?

Just read the documentation in the README file [here](https://gitlab.com/wovalab/open-source/labview-doc-generator), to learn how to install Antidoc and how to render final documents.

If you want to contribute to its improvement, let's read the CONTRIBUTING file at the same place.

We hope you'll enjoy this tool. 